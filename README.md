# k8-elasticsearch
# Configuring EKS on AWS, deploying Apache Web Server and configuring Elasticsearch (EFK Stack) 

- **Subtask I - Create Terraform scripts for AWS EKS infrasrtucture:**
  - For running `terraform apply` command, we create `.tf` files to set up VPC, security groups, EKS, variables and so on. We used official VPC and EKS modules for infrastructure. Firstly we mention variables in [variables.tf](https://gitlab.com/elshanfarzali/k8-elasticsearch/-/blob/main/k8-terraform/variables.tf).
  - To create EC2 instance on AWS we set recources on [eks.tf](https://gitlab.com/elshanfarzali/k8-elasticsearch/-/blob/main/k8-terraform/eks.tf) below:

    * aws_vpc - Provides a Virtual Private Network for AWS instance;
    * aws_subnet - I created a public subnet for EC2 instance;
    * aws_security_group - For EC2 instance to set connection policies;
    * aws_internet_gateway - To make EC2 instance have internet access;
    * aws_route_table - For being routed packets to the internet gateway;
    * aws_route_table_association - It shows packets in which subnets will be routed
    * aws_network_interface - It create network interface card for EC2;
    * EKS - AWS Kubernetes cluster. 

- **Subtask II - Gitlab:**
  - Before running pipeline we should add AWS and some other variables (for more information [gitlab-variables](https://docs.gitlab.com/ee/ci/variables/)) and configure shell gitlab-runner on local:
      ```
      gitlab-runner register -n \
      --url ${gitlab_registry} \
      --registration-token "${gitlab_token}" \
      --executor shell \
      --description "your description"
      ```
  - I created 6 jobs and one stage for each job:

    * init - It initializes terraform and installs required plugins;
    * validate - Validate that terraform config is ok;
    * plan - It creates execution plan for next stage;
    * apply - Apply terraform based on execution plan;
    * deploy - Deploy kubernetes manifest files;
    * destroy - This stage running manually to destroy config if there is need;

# Note:
- **For accesing kubernetes dashboard:**

    * Get security token:
        ```
        kubectl -n kubernetes-dashboard get secret $(kubectl -n kubernetes-dashboard get sa/dashboard-admin -o jsonpath="{.secrets[0].name}") -o go-template="{{.data.token | base64decode}}"
        ```


    * Create tunnel:
        `kubectl proxy`


    * Access DASHBOARD:
        [K8 Dashboard](http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/)
